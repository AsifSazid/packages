<?php
namespace Sazid\Providers;

use Illuminate\Support\ServiceProvider;

class SazidServiceProvider extends ServiceProvider
{
    public function register()
    {

    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
    }

}
    

